# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.1.9"></a>
## [1.1.9](https://bitbucket.org/millionspaces/test-git-commits/compare/v1.1.8...v1.1.9) (2019-02-16)



<a name="1.1.8"></a>

## [1.1.8](https://bitbucket.org/millionspaces/test-git-commits/compare/v1.1.7...v1.1.8) (2019-02-16)

<a name="1.1.7"></a>

## [1.1.7](https://bitbucket.org/millionspaces/test-git-commits/compare/v1.1.6...v1.1.7) (2019-02-16)

<a name="1.1.6"></a>

## [1.1.6](https://bitbucket.org/millionspaces/test-git-commits/compare/v1.1.5...v1.1.6) (2019-02-16)

<a name="1.1.5"></a>

## [1.1.5](https://bitbucket.org/millionspaces/test-git-commits/compare/v1.1.4...v1.1.5) (2019-02-16)

<a name="1.1.4"></a>

## [1.1.4](https://bitbucket.org/millionspaces/test-git-commits/compare/v1.1.3...v1.1.4) (2019-02-16)

<a name="1.1.3"></a>

## [1.1.3](https://bitbucket.org/millionspaces/test-git-commits/compare/v1.1.2...v1.1.3) (2019-02-16)

<a name="1.1.2"></a>

## [1.1.2](https://bitbucket.org/millionspaces/test-git-commits/compare/v1.1.1...v1.1.2) (2019-02-16)

<a name="1.1.1"></a>

## [1.1.1](https://bitbucket.org/millionspaces/test-git-commits/compare/v1.1.0...v1.1.1) (2019-02-16)

<a name="1.1.0"></a>

# 1.1.0 (2019-02-16)

### Features

-   **foo.ts:** class A has been implemented ([ce7a49b](https://bitbucket.org/millionspaces/test-git-commits/commits/ce7a49b))

### Performance Improvements

-   **package.json:** husky changes added ([f34dd29](https://bitbucket.org/millionspaces/test-git-commits/commits/f34dd29))

<a name="1.0.1"></a>

## 1.0.1 (2019-02-16)

### Performance Improvements

-   **package.json:** husky changes added ([f34dd29](https://bitbucket.org/millionspaces/test-git-commits/commits/f34dd29))
